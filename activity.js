//1
db.users.find( { $or: [{firstName: {$regex: 's',$options: 'i'}}, { lastName: {$regex: 'd',$options: 'i'}}] }, {"age":0, _id:0 , "courses": 0, "contact":0, "department":0} ).pretty();

/*
{
    "firstName" : "Jane",
    "lastName" : "Doe"
}

{
    "firstName" : "Stephen",
    "lastName" : "Hawking"
}

*/






//2

db.users.find( { $and: [{"department": "HR"}, {"age":{$gte:70}}] }, { "age":0,_id:0 , "courses": 0, "contact":0, "department":0} ).pretty();

/*{
    "firstName" : "Stephen",
    "lastName" : "Hawking"
}


{
    "firstName" : "Neil",
    "lastName" : "Armstrong"
}


*/

//3
db.users.find( { $and: [{firstName: {$regex: 'e',$options: 'i'}}, { lastName: {$regex: 'e',$options: 'i'}},{"age":{$lte:30}} ]}, {"age":0, _id:0 , "courses": 0, "contact":0, "department":0} ).pretty();

/*{
    "firstName" : "Jane",
    "lastName" : "Doe"
}

*/